package com.tasks;

public class Tasks {
    // задание 1. Удаление лишних пробелов
    public String task1(String s) {
        String result = "";
        if (s.length() > 0) {
            String[] temp;
            temp = s.split(" ");
            for (String k : temp) {
                if (!k.equals("")) {
                    result += k + " ";
                }
            }
            result = result.trim();
        }
        return result;
    }

    // задание 2. Найти кол-во гласных и согласных букв в тексте
    public String task2(String s) {
        int vowelsCount = 0, consistencyCount = 0;
        String result = "empty row";
        if (s.length() > 0) {
            String vowels = "AEIOUYaeiouy";
            String consistency = "BCDFGHJKLMNPQRSTVWXZbcdfghjklmnpqrstvwxz";
            for (int i = 0; i < s.length(); i++) {
                if (vowels.indexOf(s.charAt(i)) != -1) {
                    vowelsCount++;
                }
                if (consistency.indexOf(s.charAt(i)) != -1) {
                    consistencyCount++;
                }
            }
        }
        result = "vowelsCount: " + vowelsCount + "\nconsistencyCount: " + consistencyCount;

        return result;
    }

    // задание 3. Найти в тексте слова палиндромы
    public String task3(String s) {
        String result = "";
        if (s.length() > 0) {
            String[] temp;
            temp = s.split(" ");
            for (String k : temp) {
                if (!k.equals("") && new StringBuilder(k.toLowerCase()).toString().equals(new StringBuilder(k.toLowerCase()).reverse().toString())) {
                    System.out.println(k);
                }

            }

        }
        return result;
    }

    // задание 4. Вывести все слова текста в обратном порядке
    public String task4(String s) {
        StringBuilder result = new StringBuilder();

        String symbols = s.replaceAll("[^!.?]", "");
        String[] sentences = s.split("[.!?]");
        for (int i = 0; i < sentences.length; i++) {
            String sentence = sentences[i].trim();
            String[] words = sentence.split(" ");
            if (words.length > 0) {
                for (int j = 0; j < words.length/2 ; j++) {
                    String temp = words[j];
                    words[j] = words[words.length - 1 - j];
                    words[words.length - 1 - j] = temp;
                }

            }
            sentence = String.join(" ", words);
            result.append(sentence).append(symbols.charAt(i)).append(" ");
        }
        return result.toString().trim();

    }

    // задание 5. Удалить все слова из текста, у которых длинна равна n
    public String task5(String s, int n) {
        StringBuilder result = new StringBuilder();

        String symbols = s.replaceAll("[^!.?]", "");
        String[] sentences = s.split("[.!?]");
        for (int i = 0; i < sentences.length; i++) {
            String sentence = sentences[i].trim();
            String[] words = sentence.split(" ");
            if (words.length > 0) {
                for (int j = 0; j < words.length; j++) {
                    if (words[j].length() == n) {
                        words[j] = "";
                    }
                }
            }
            sentence = String.join(" ", words);
            result.append(sentence).append(symbols.charAt(i)).append(" ");
            ;


        }
        return result.toString().trim();
    }

    // задание 6. Найти кол-во слов, которые начинаются и заканчиваются
    //одинаковой буквой. Букву передавайте в параметрах.
    public int task6(String s, char letter) {
        int count = 0;
        String[] sentences = s.split("[.!?]");
        for (int i = 0; i < sentences.length; i++) {
            String sentence = sentences[i].trim();
            String[] words = sentence.split(" ");
            if (words.length > 0) {
                for (int j = 0; j < words.length; j++) {
                    if (words[j].length() > 0) {
                        if (words[j].charAt(0) == letter && words[j].charAt(words[j].length() - 1) == letter) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    // задание 7. Найти все слова максимальной и минимальной длинны
    public void task7(String data) {
        String[] words = data.split(" ");
        if (words.length > 0) {
            int minLength = words[0].length();
            int maxLength = words[0].length();
            StringBuilder minWords = new StringBuilder();
            StringBuilder maxWords = new StringBuilder();
            for (String word : words) {
                if (minLength > word.length()) {
                    minLength = word.length();
                    minWords.delete(0, minWords.length() - 1);
                }
                if (maxLength < word.length()) {
                    maxLength = word.length();
                    maxWords.delete(0, maxWords.length() - 1);
                }
                if (minLength == word.length()) {
                    minWords.append(word).append(" ");
                }
                if (maxLength == word.length()) {
                    maxWords.append(word).append(" ");
                }
            }
            System.out.println("min: " + minWords);
            System.out.println("max: " + maxWords);
        }
    }





    // задание 8. Во всех предложениях поменять местами первое и последнее слово.
    public String task8(String s){

        return null;
    }

    // задание 9. Заменить рядом стоящие одинаковые символы на один символ
    public String task9(String s){
        char curr, prev;
        String result = "";
        if (s.length() > 1){
            StringBuilder builder = new StringBuilder(s);

            for (int i = 1; i < builder.length() ; i++) {
                prev = builder.charAt(i-1);
                curr = builder.charAt(i);
                if (curr == prev){
                    builder.deleteCharAt(i-1);
                    i--;
                }

            }
        result = builder.toString();
        }

        return result;
    }

    // задание 10. Каждый n-й символ заменить заданным символом.
    public String task10(){
        return null;
    }

    // задание 11. Удалить из текста все что заключено в круглые скобки.
    public String task11(String s){
        return  s.replaceAll("(\\(.+\\))","");
        /*
        String result = "";
        if (s.length() > 1){
            StringBuilder builder = new StringBuilder(s);
            int start = 0;

            for (int i = 1; i < builder.length() ; i++) {
                if (builder.charAt(i) == '(' && start == 0){
                    start = i;
                }
                if (builder.charAt(i) == ')' && start != 0){
                    builder.delete(start, i+1);
                    i = start;
                    start = 0;
                }

            }
            result = builder.toString();
        }

        return result;

         */
    }
    // задание 12.Форматирование текста в следующем виде: так, чтобы все абзацы
    //имели отступ ровно 4 пробела, каждое предложение начиналось с заглавной
    //буквы, а длина каждой строки была не более 120 символов и не имела
    //начальными и конечными символами пробельный символ.
    public void task12(){
        TextBuilder b = new TextBuilder2("D:\\1.txt");
        b.readFile();
        b.textFormatter();

    }

    // задание 13. Найти кол-во повторений каждого слова в тексте. (Нужно
    //использовать Map)
    public String task13(){
        return null;
    }

    public static void main(String[] args) {
        Tasks t = new Tasks();
        t.task12();
    }
}
