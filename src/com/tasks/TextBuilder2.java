package com.tasks;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextBuilder2 extends TextBuilder {
    public TextBuilder2(String file) {
        super(file);
    }

    // задание 12.Форматирование текста в следующем виде: так, чтобы все абзацы
    //имели отступ ровно 4 пробела, каждое предложение начиналось с заглавной
    //буквы, а длина каждой строки была не более 120 символов и не имела
    //начальными и конечными символами пробельный символ.
    @Override
    public void textFormatter(int rowLen) {
        StringBuffer result = new StringBuffer();
        Iterator<String> i = paragraphs.iterator();
        while (i.hasNext()) {
            String curr =  i.next().trim();
            String symbols = curr.replaceAll("[^?.!]", "");
            String[] sentences = curr.split("[?!.]");
            for (int j = 0; j < sentences.length; j++) {
                StringBuilder sentence = new StringBuilder(sentences[j].trim());
                String reg = "[а-я[a-z]].+";
                Pattern p2 = Pattern.compile(reg);
                Matcher m2 = p2.matcher(sentence);

                if (m2.matches()) {
                    String temp =  String.valueOf(sentence.charAt(0));
                    temp = temp.toUpperCase();
                    sentence.setCharAt(0,temp.charAt(0));
                }
                if (j == 0){
                    sentence.insert(0,"\n    ");
                }

                result.append(sentence);
                result.append(symbols.charAt(j));
            }

        }
        result.deleteCharAt(0);
        int tab = 4;
        String reg3 = "(\\s{"+tab+"}.{"+(rowLen-tab)+"})|(.{"+(rowLen-1)+"}\\S)|(.{"+rowLen+"})";
        Pattern p3 = Pattern.compile(reg3);
        Matcher m3 = p3.matcher(result);
        while (m3.find()){
            System.out.println(m3.group());
        }
       //System.out.println(result);
    }
}
