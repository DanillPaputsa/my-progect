package com.tasks.chapter2.wow.classes;

import com.tasks.chapter2.wow.Fraction;
import com.tasks.chapter2.wow.Race;

import java.util.Objects;

public abstract class WOWCharacter {
    public static int id = 0;
     String nickName;
     boolean sex;
     private Race race;
     private Fraction fraction;
     int level;
     int healthPoint;
     int manna;
     int power;

    {
        nickName = "new_Character_" + ++id;
        sex = true;
        race = Race.HUMAN;
        fraction = Fraction.HORDE;
        level = 1;
        healthPoint = 1;
        manna = 1;
        power = 1;
        Fraction.HORDE.getList().add(this);
    }

    public WOWCharacter(){

    }



    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race.getList().remove(this);
        this.race = race;
        this.race.getList().add(this);

    }

    public Fraction getFraction() {
        return fraction;
    }

    public void setFraction(Fraction fraction) {
        this.fraction.getList().remove(this);
        this.fraction = fraction;
        this.fraction.getList().add(this);
    }

    public void say(String text){
        System.out.println("\"" +nickName+"\" : " +text);
    }

    public abstract void attack(WOWCharacter target);

    public abstract void move();

    public abstract void sleep();


    public void laugh(){
       say(" LOL");
    }

    public void laugh(WOWCharacter target){
        say(" laughs at " + "\"" + target.nickName + "\"!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WOWCharacter that = (WOWCharacter) o;
        return sex == that.sex &&
                level == that.level &&
                healthPoint == that.healthPoint &&
                manna == that.manna &&
                power == that.power &&
                nickName.equals(that.nickName) &&
                race == that.race &&
                fraction == that.fraction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickName, sex, race, fraction, level, healthPoint, manna, power);
    }


}
