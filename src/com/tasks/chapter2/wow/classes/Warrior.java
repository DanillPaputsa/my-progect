package com.tasks.chapter2.wow.classes;

import com.tasks.chapter2.wow.Fraction;
import com.tasks.chapter2.wow.Race;

public class Warrior extends WOWCharacter {

    {
        healthPoint = 150;
        manna = -1;
        power = 25;
    }

    public Warrior(){
        super();
    }

    public Warrior(String nickName, boolean sex, Race race, Fraction fraction){
        this.nickName = nickName;
        this.sex = sex;
        setRace(race);
        setFraction(fraction);
    }





    @Override
    public void attack(WOWCharacter target) {
        int damage = power + level;
        target.healthPoint -= damage;
        say(" attack the "+target.nickName +". Total damage = " + damage);
    }

    @Override
    public void move() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public String toString() {
        return "Warrior{" +
                "nickName='" + nickName + '\'' +
                ", sex=" + sex +
                ", level=" + level +
                ", healthPoint=" + healthPoint +
                ", manna=" + manna +
                ", power=" + power +
                ", race=" + this.getRace() +
                ", fraction=" + this.getFraction() +
                '}';
    }
}
