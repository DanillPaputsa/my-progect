package com.tasks.chapter2.wow.classes;

import com.tasks.chapter2.wow.Fraction;
import com.tasks.chapter2.wow.Race;

public class Mage extends WOWCharacter {

    {
        healthPoint = 80;
        manna = 200;
        power = 20;
    }

    public Mage(){
        super();
    }

    public Mage(String nickName, boolean sex, Race race, Fraction fraction){
        this.nickName = nickName;
        this.sex = sex;
        setRace(race);
        setFraction(fraction);
    }

    @Override
    public void attack(WOWCharacter target) {
        if (manna <=0){
            say(" empty manna!");
        }else{
            manna -= 12;
            int damage = power + level +(int)(manna * 0.3);
            target.healthPoint -= damage;
            say(" attack the "+target.nickName +". Total damage = " + damage);
        }


    }

    @Override
    public void move() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public String toString() {
        return "Mage{" +
                "nickName='" + nickName + '\'' +
                ", sex=" + sex +
                ", level=" + level +
                ", healthPoint=" + healthPoint +
                ", manna=" + manna +
                ", power=" + power +
                ", race=" + this.getRace() +
                ", fraction=" + this.getFraction() +
                '}';
    }
}
