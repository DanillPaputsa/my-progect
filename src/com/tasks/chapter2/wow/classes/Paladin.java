package com.tasks.chapter2.wow.classes;

import com.tasks.chapter2.wow.Fraction;
import com.tasks.chapter2.wow.Race;

public class Paladin extends WOWCharacter {

    {
        healthPoint = 100;
        manna = 30;
        power = 15;
    }

    public Paladin(){
        super();
    }

    public Paladin(String nickName, boolean sex, Race race, Fraction fraction){
        this.nickName = nickName;
        this.sex = sex;
        setRace(race);
        setFraction(fraction);
    }


    @Override
    public void attack(WOWCharacter target) {
        if (manna <=0){
            say(" empty manna!");
        }else{
            manna -= 10;
            int damage = power + level +(int)(manna * 0.1);
            target.healthPoint -= damage;
            say(" attack the "+target.nickName +". Total damage = " + damage);
        }

    }

    @Override
    public void move() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public String toString() {
        return "Paladin{" +
                "nickName='" + nickName + '\'' +
                ", sex=" + sex +
                ", level=" + level +
                ", healthPoint=" + healthPoint +
                ", manna=" + manna +
                ", power=" + power +
                ", race=" + this.getRace() +
                ", fraction=" + this.getFraction() +
                '}';
    }
}
