package com.tasks.chapter2.wow;

import com.tasks.chapter2.wow.classes.WOWCharacter;

import java.util.ArrayList;

public enum Race {
    HUMAN("HUMAN", new ArrayList<WOWCharacter>()),
    ELF("ELF", new ArrayList<WOWCharacter>());

    private String name;
    private ArrayList<WOWCharacter> list;


    Race(String name, ArrayList<WOWCharacter> list){
        this.name = name;
        this.list = list;
    }


    public String getName() {
        return name;
    }

    public ArrayList<WOWCharacter> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Race{" +
                "name='" + name + '\'' +
                '}';
    }
}
