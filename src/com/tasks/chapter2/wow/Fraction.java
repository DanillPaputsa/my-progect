package com.tasks.chapter2.wow;

import com.tasks.chapter2.wow.classes.WOWCharacter;

import java.util.ArrayList;

public enum Fraction {
    HORDE("HORDE", new ArrayList<WOWCharacter>()),
    ALLIANCE("ALLIANCE", new ArrayList<WOWCharacter>());

    private String name;
    private ArrayList<WOWCharacter> list;

    Fraction(String name, ArrayList<WOWCharacter> list){
        this.name = name;
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public ArrayList<WOWCharacter> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Fraction{" +
                "name='" + name + '\'' +
                '}';
    }
}
