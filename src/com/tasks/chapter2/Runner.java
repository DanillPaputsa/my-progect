package com.tasks.chapter2;

import com.tasks.chapter2.wow.Fraction;
import com.tasks.chapter2.wow.Race;
import com.tasks.chapter2.wow.classes.Mage;
import com.tasks.chapter2.wow.classes.Paladin;
import com.tasks.chapter2.wow.classes.WOWCharacter;
import com.tasks.chapter2.wow.classes.Warrior;

import java.util.ArrayList;

public class Runner {
    public static void main(String[] args) {
        CharacterWorker worker = new CharacterWorker();
        worker.generate(10);
        worker.doSomething();

        Paladin paladin = new Paladin("Admin", true , Race.ELF,Fraction.ALLIANCE);
        Mage mage = new Mage("Game master", false , Race.HUMAN, Fraction.HORDE);
        Warrior mage1 = new Warrior("Game master1", false , Race.ELF, Fraction.ALLIANCE);
        Paladin mage2 = new Paladin("Game master2", false , Race.HUMAN, Fraction.HORDE);
        Mage mage3 = new Mage("Game master3", false , Race.ELF, Fraction.ALLIANCE);
        Warrior mage4 = new Warrior("Game master4", false , Race.HUMAN, Fraction.HORDE);

        System.out.println("Race ELF Characters: ");
        printArrayList(Race.ELF.getList());

        System.out.println("Race HUMAN Characters: ");
        printArrayList(Race.HUMAN.getList());


    }

    public static void printArrayList(ArrayList<WOWCharacter> list){
        for(WOWCharacter p: list){
            System.out.println(p);
        }
        System.out.println("\n");
    }
}
