package com.tasks.chapter2;

import com.tasks.chapter2.wow.Race;
import com.tasks.chapter2.wow.classes.Mage;
import com.tasks.chapter2.wow.classes.Paladin;
import com.tasks.chapter2.wow.classes.WOWCharacter;
import com.tasks.chapter2.wow.classes.Warrior;

import java.util.ArrayList;

public  class CharacterWorker {
    ArrayList<WOWCharacter> list = new ArrayList<>();

    public CharacterWorker(){

    }

    public void doSomething(){
        for(WOWCharacter p: list){
            System.out.println(p);
            p.attack(p);
            System.out.println(p);
            p.say(" Hello!");
            p.laugh();
            p.laugh(p);
            System.out.println("\n");
        }
    }

    public void generate(int count){
        int k = -1;
        for (int i = 0; i < count; i++){
            k = (int)(Math.random()*(3));
            switch (k){
                case 0:{
                    list.add(new Warrior());
                    break;
                }
                case 1:{
                    list.add(new Mage());
                    break;
                }
                case 2:{
                    list.add(new Paladin());
                    break;
                }
                default:
                    break;
            }
        }
    }

}
