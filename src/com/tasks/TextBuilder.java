package com.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextBuilder {
    List<String> paragraphs ;
    String file = "";



    public TextBuilder(String file) {
        this.file = file;
        this.paragraphs = readFile(file);
    }

    public List<String> readFile() {
        return readFile(file);
    }

    public List<String> readFile(String input) {
        Scanner s = null;
        List<String> paragraphs = new ArrayList<String>();

        try {
            s = new Scanner(new File(input));
            s.useDelimiter("\n");
            while(s.hasNext()) {
                paragraphs.add(s.next());
            }

        } catch (FileNotFoundException e) {
            System.err.println("file error: " + e);
        }

        return paragraphs;

    }
    public void textFormatter() {
        textFormatter(120);
    }


    public  void textFormatter(int rowLen) {
        StringBuffer result = new StringBuffer();
        Iterator<String> i = paragraphs.iterator();
        while (i.hasNext()) {
            String curr =  i.next().trim();
            String symbols = curr.replaceAll("[^?.!]", "");
            String[] sentences = curr.split("[?!.]");
            for (int j = 0; j < sentences.length; j++) {
                StringBuilder sentence = new StringBuilder(sentences[j].trim());
                String reg = "[à-ÿ[a-z]].+";
                Pattern p2 = Pattern.compile(reg);
                Matcher m2 = p2.matcher(sentence);

                if (m2.matches()) {
                    String temp =  String.valueOf(sentence.charAt(0));
                    temp = temp.toUpperCase();
                    sentence.setCharAt(0,temp.charAt(0));
                }

                if (j == 0){
                    sentence.insert(0,"\n    ");
                }
                result.append(sentence);
                result.append(symbols.charAt(j));
            }

        }
        result.deleteCharAt(0);

        for (int k = 0, u = 0; k < result.length(); k++, u++) {
            if (result.charAt(k)=='\n') {
                u=0;
            }
            if (u == rowLen-1) {
                result.insert(k, '\n');
                u = 0;
            }
        }
        System.out.println(result);

    }




}